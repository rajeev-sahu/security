package security.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import security.security.services.AuthService;
import security.security.services.DomainDetailService;
import security.security.services.DomainName;

@RestController
@RequestMapping(path = "/domains")
public class DomainController {

    private final DomainDetailService domainDetailService;

    private final AuthService authService;

    @Autowired
    public DomainController(final DomainDetailService domainDetailService,
                            final AuthService authService) {

        this.domainDetailService = domainDetailService;
        this.authService = authService;
    }

    @GetMapping("{domainName:.+}")
    public String getPoolName(@PathVariable(name = "domainName") final String domainName,
                              @RequestHeader(name = "user-id") final Long userId) {

//        authService.authenticateOrderOfUser(userId, new DomainName(domainName));

        domainDetailService.getPoolName(new DomainName(domainName));
        domainDetailService.getPoolName(domainName);
        domainDetailService.getPoolName(10L);
        return domainDetailService.getPoolName(new DomainRequest(domainName));
    }

    @PostMapping("/register")
    public String register(@RequestBody final DomainRequest request,
                           @RequestHeader(name = "user-id") final Long userId) {

//        authService.authenticateOrderOfUser(userId, new DomainName(domainName));

        domainDetailService.getPoolName(new DomainName(request.getDomainName()));
        domainDetailService.getPoolName(request.getDomainName());
        domainDetailService.getPoolName(10L);
        domainDetailService.getPoolName(request);

        return "RETURNED FROM POST";
    }


}