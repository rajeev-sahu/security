package security.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import security.security.services.AccountService;

@RestController
@RequestMapping(path = "/accounts")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(final AccountService accountService) {

        this.accountService = accountService;
    }

    @PostMapping("/transfer")
    public void register(@RequestBody final FundTransferRequest fundTransferRequest,
                         @RequestHeader(name = "user-id") final Long userId) {

        accountService.transfer(fundTransferRequest.getFromAccountId(),
                                fundTransferRequest.getToAccountId(),
                                fundTransferRequest.getBalanace());
    }
}