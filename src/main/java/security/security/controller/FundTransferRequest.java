package security.security.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FundTransferRequest {

    private Long fromAccountId;
    private Long toAccountId;
    private BigDecimal balanace;


}
