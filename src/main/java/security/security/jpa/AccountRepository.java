package security.security.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import security.security.entities.Account;
import security.security.entities.Domain;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findByAccountId(final Long accountId);

//    @Query(value = "UPDATE account a SET a.balance = ?2 WHERE a.fromAccount = ?1")
//    void updateBalance(final Long fromAccount,final BigDecimal newBalance);

}
