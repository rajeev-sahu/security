package security.security.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import security.security.entities.AuditLog;

public interface AuditLogRepository extends JpaRepository<AuditLog, Long> {

}
