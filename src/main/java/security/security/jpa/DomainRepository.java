package security.security.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import security.security.entities.Domain;

import java.util.List;
import java.util.Optional;

public interface DomainRepository extends JpaRepository<Domain, Long> {

    Optional<Domain> findByDomainName(final String domainName);

    List<Domain> findByDomainNameAndUserIdAndStatusNot(final String domainName,
                                                       final Long userId,
                                                       final String status);

}
