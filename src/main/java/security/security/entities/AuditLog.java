package security.security.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
public class AuditLog extends BasicEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private Long fromAccount;

    @Column(nullable = false)
    private Long toAccount;

    @Column(nullable = false)
    private BigDecimal amount;

    public AuditLog(final Long fromAccount,
                    final Long toAccount,
                    final BigDecimal amount) {

        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.amount = amount;
    }
}
