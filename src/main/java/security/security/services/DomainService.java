package security.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import security.security.jpa.DomainRepository;

@Service
public class DomainService {

    private final DomainRepository domainRepository;

    @Autowired
    public DomainService(final DomainRepository domainRepository) {

        this.domainRepository = domainRepository;
    }

    @PreAuthorize("isDomainOfCurrentUser(#domainName)")
    public String getPoolName(final DomainName domainName) {

        System.out.println("Entering getPoolName");

        publicMethod(domainName.getDomainName());
        privateMethod(domainName.getDomainName());

        return domainRepository.findByDomainName(domainName.getDomainName())
                               .orElseThrow(RuntimeException::new)
                               .getPoolName();
    }

    @PreAuthorize("isDomainOfCurrentUser(#domainName)")
    private void privateMethod(final String domainName) {

        System.out.println("Private method");
    }

    @PreAuthorize("isDomainOfCurrentUser(#domainName)")
    private void publicMethod(final String domainName) {

        System.out.println("Public method");
    }
}