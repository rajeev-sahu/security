package security.security.services;

import org.springframework.stereotype.Service;
import security.security.entities.Account;
import security.security.entities.AuditLog;
import security.security.jpa.AccountRepository;
import security.security.jpa.AuditLogRepository;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class AccountService {

    private final AccountRepository accountRepository;

    private final AuditLogRepository auditLogRepository;

    public AccountService(final AccountRepository accountRepository,
                          final AuditLogRepository auditLogRepository) {

        this.accountRepository = accountRepository;
        this.auditLogRepository = auditLogRepository;
    }

    public void transfer(final Long fromAccountId,
                         final Long toAccountId,
                         final BigDecimal amount) {

        final Optional<Account> fromAccount = accountRepository.findByAccountId(fromAccountId);

        final Optional<Account> toAccount = accountRepository.findByAccountId(toAccountId);

        if (fromAccount.isPresent() && toAccount.isPresent()) {

            final Account fromAccount1 = fromAccount.get();
            final Account toAccount1 = toAccount.get();

            final BigDecimal fromAccountCurrentBalance = fromAccount1.getBalance();

            final BigDecimal toAccountCurrentBalance = toAccount1.getBalance();

            fromAccount1.setBalance(fromAccountCurrentBalance.subtract(amount));

            toAccount1.setBalance(toAccountCurrentBalance.add(amount));

            auditLogRepository.save(new AuditLog(fromAccountId, toAccountId, new BigDecimal("0.00")));
        }
    }

}
