package security.security.services;

public enum DomainStatus {
    ACTIVE,
    INACTIVE,
    DELETED,
    QUEUED_FOR_DELETION,
    PENDING_DELETE_RESTORABLE,
    SUSPENDED

}
