package security.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import security.security.entities.Domain;
import security.security.jpa.DomainRepository;

import java.util.List;

@Service
@Transactional
public class AuthDomainDetailService {

    private final DomainRepository domainRepository;

    @Autowired
    public AuthDomainDetailService(final DomainRepository domainRepository) {

        this.domainRepository = domainRepository;
    }

    public List<Domain> getNonDeletedDomains(final Long userId,
                                             final String domainName) {

        return domainRepository.findByDomainNameAndUserIdAndStatusNot(domainName,
                                                                      userId,
                                                                      "Deleted");
    }
}
