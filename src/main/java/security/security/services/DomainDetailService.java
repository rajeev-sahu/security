package security.security.services;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import security.security.controller.DomainRequest;
import security.security.jpa.DomainRepository;

@Service
@Transactional
public class DomainDetailService {

    private final DomainRepository domainRepository;

    public DomainDetailService(DomainRepository domainRepository) {

        this.domainRepository = domainRepository;
    }

    @PreAuthorize("isDomainOfCurrentUser(#domainRequest.domainName)")
    public String getPoolName(final DomainRequest domainRequest) {

        return "getPoolName-DomainRequest " + domainRequest;
    }

    @PreAuthorize("isDomainOfCurrentUser(#domainName)")
    public String getPoolName(final DomainName domainName) {

        return "getPoolName-DomainName " + domainName;
    }


    @PreAuthorize("isDomainOfCurrentUser(#domainName)")
    public String getPoolName(final String domainName) {

        System.out.println("Entering getPoolName");

        return "getPoolName-domainName " + domainName;
    }


    @PreAuthorize("isDomainOfCurrentUser(#orderId)")
    public String getPoolName(final Long orderId) {

        return "getPoolName-orderId " + orderId;
    }


}
