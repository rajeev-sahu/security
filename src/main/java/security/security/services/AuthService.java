package security.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import security.security.entities.Domain;

import java.util.List;

@Service
public class AuthService {

    private final AuthDomainDetailService authDomainDetailService;

    @Autowired
    public AuthService(final AuthDomainDetailService authDomainDetailService) {

        this.authDomainDetailService = authDomainDetailService;
    }
    public void authenticateOrderOfUser(final Long userId,
                                        final DomainName domainName) {

        final List<Domain> nonDeletedDomains = authDomainDetailService.getNonDeletedDomains(userId, domainName.getDomainName());

        if (nonDeletedDomains.isEmpty()) {

            throw new RuntimeException();
        }
    }

}
