package security.security.config;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import security.security.services.AuthService;

@Component
public class CoreMethodSecurityExpressionHandler extends DefaultMethodSecurityExpressionHandler
{
    private final AuthService authService;

    private AuthenticationTrustResolver trustResolver = new AuthenticationTrustResolverImpl();

    public CoreMethodSecurityExpressionHandler(final AuthService authService)
    {
        this.authService = authService;
    }

    @Override
    protected MethodSecurityExpressionOperations createSecurityExpressionRoot(Authentication authentication,
                                                                              MethodInvocation invocation)
    {
        CoreMethodSecurityExpressionRoot root = new CoreMethodSecurityExpressionRoot(authentication,
                                                                                     authService);
        root.setPermissionEvaluator(getPermissionEvaluator());
        root.setTrustResolver(this.trustResolver);
        root.setRoleHierarchy(getRoleHierarchy());
        return root;
    }
}
