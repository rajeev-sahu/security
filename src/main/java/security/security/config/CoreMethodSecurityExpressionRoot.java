package security.security.config;

import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import security.security.services.AuthService;
import security.security.services.DomainName;

public class CoreMethodSecurityExpressionRoot extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {

    private final AuthService authService;

    CoreMethodSecurityExpressionRoot(final Authentication authentication,
                                     final AuthService authService) {

        super(authentication);
        this.authService = authService;
    }

    @Override
    public void setFilterObject(Object o) {

    }

    @Override
    public Object getFilterObject() {

        return null;
    }

    @Override
    public void setReturnObject(Object o) {

    }

    @Override
    public Object getReturnObject() {

        return null;
    }

    @Override
    public Object getThis() {

        return null;
    }

    public Boolean isDomainOfCurrentUser(final DomainName domainName) {

        System.out.println("Entering isDomainOfCurrentUser - DomainName " + domainName);

        final String userId = ((User) authentication.getPrincipal()).getUsername();
        authService.authenticateOrderOfUser(Long.parseLong(userId), domainName);

        return Boolean.TRUE;
    }

    public Boolean isDomainOfCurrentUser(final String domainName) {

        System.out.println("Entering isDomainOfCurrentUser - String " + domainName);
        return Boolean.TRUE;
    }

    public Boolean isDomainOfCurrentUser(final Long orderId) {

        System.out.println("Entering isDomainOfCurrentUser - Long " + orderId);
        return Boolean.TRUE;


    }
}
